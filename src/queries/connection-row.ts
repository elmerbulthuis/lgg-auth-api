import * as authDb from "@latency.gg/lgg-auth-db";
import * as authSpec from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { convertPgBinaryToBuffer, createRowQueryFactory, QuerySource, RowQueryState } from "../utils/index.js";

//#region query

export type ConnectionRowQuery = InstanceMemoizer<
    Promise<ConnectionRowQuerySource>, [Partial<authDb.ConnectionRow>]
>

export type ConnectionRowQuerySource = QuerySource<
    ConnectionRowState, authDb.ConnectionRowEvent
>

export function createConnectionRowQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "pgPool">,
    onError: (error: unknown) => void,
): ConnectionRowQuery {
    return createRowQueryFactory({
        pool: services.pgPool,
        getPrimaryKey: key => makePrimaryKey(key),
        getIndexKey: {},
        onError,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
        createTableEvents: authDb.createConnectionRowEvents,
    });

}

//#endregion

//#region state / events

export type ConnectionRowState = RowQueryState<authDb.ConnectionRow>;

//#endregion

//#region selectors

export function selectConnectionRow(
    state: ConnectionRowState,
    provider: string,
    subject: string,
    user: string,
): authDb.ConnectionRow | undefined {
    const key = makePrimaryKey({ provider, subject, user_id: user });
    const row = state.rows.get(key);
    if (!row) return;

    return row;
}

export function createConnectionSnapshotEventFromRowState(
    state: ConnectionRowState,
): authSpec.ConnectionSnapshotSchema {
    const payload = {
        connections: state.rows.
            map(row => mapRowToEntity(row)).
            valueSeq().
            toArray(),
    };

    return {
        type: "connection-snapshot",
        payload,
    };
}

export function createConnectionCreatedEventFromRowState(
    state: ConnectionRowState,
    provider: string,
    subject: string,
    user: string,
): authSpec.ConnectionCreatedSchema {
    const row = selectConnectionRow(state, provider, subject, user);
    assert(row != null, "expected row");

    const payload = {
        connection: mapRowToEntity(row),
    };

    return {
        type: "connection-created",
        payload,
    };
}

export function createConnectionDeletedEventFromRowState(
    state: ConnectionRowState,
    provider: string,
    subject: string,
    user: string,
): authSpec.ConnectionDeletedSchema {
    const row = selectConnectionRow(state, provider, subject, user);
    assert(row != null, "expected row");

    const payload = {
        connection: mapRowToEntity(row),
    };

    return {
        type: "connection-deleted",
        payload,
    };
}

//#endregion

//#region helpers

function makePrimaryKey(key: authDb.ConnectionRowKey) {
    return [key.provider, key.subject, key.user_id].join("\x1f");
}

function mapRowToEntity(
    row: authDb.ConnectionRow,
): authSpec.ConnectionEntitySchema {
    const { subject } = row;
    const user = convertPgBinaryToBuffer(row.user_id).toString("base64");
    const created = new Date(row.created_utc).toISOString();

    return {
        subject,
        user,
        created,
    };
}

//#endregion
