import * as authSpec from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import deepEqual from "fast-deep-equal";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { convertPgBinaryFromBuffer, createQueryFactory, ErrorEvent, isErrorEvent, QuerySource } from "../utils/index.js";
import * as clientSecretRow from "./client-secret-row.js";

//#region query

export type ClientSecretQuery = InstanceMemoizer<
    Promise<ClientSecretQuerySource>, [string]
>

export type ClientSecretQuerySource = QuerySource<
    ClientSecretState, ClientSecretEventUnion
>

export function createClientSecretQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "clientSecretRowQuery">,
    onError: (error: unknown) => void,
): ClientSecretQuery {
    return createQueryFactory({
        initialState, reduce,
        createSource,
        calculateHash: client => client,
        onError,
        settle: () => true,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
        client: string,
    ) {
        const clientBuffer = Buffer.from(client, "base64");
        const clientPgBinary = convertPgBinaryFromBuffer(clientBuffer);
        const queryPromise = services.clientSecretRowQuery.acquire({
            client_id: clientPgBinary,
        });
        try {
            /**
             * wait for the query has sent it's first event, this will resolve immediately if
             * this query was already setup by an earlier require
             */
            const query = await queryPromise;

            let lastState = query.getState();

            /*
            Always emit a snaphot first
            */
            yield clientSecretRow.createClientSecretSnapshotEventFromRowState(lastState);

            /*
            then the rest of the events
            */
            for await (const { state, event } of query.fork(signal)) {
                if (isErrorEvent(event)) {
                    break;
                }

                switch (event.type) {
                    case "snapshot": {
                        const nextEvent =
                            clientSecretRow.createClientSecretSnapshotEventFromRowState(
                                state,
                            );
                        const prevEvent =
                            clientSecretRow.createClientSecretSnapshotEventFromRowState(
                                lastState,
                            );

                        if (!deepEqual(nextEvent, prevEvent)) {
                            yield nextEvent;
                        }

                        break;
                    }

                    case "insert": {
                        const digest = event.key.digest;
                        const client = event.key.client_id;
                        const nextEvent =
                            clientSecretRow.createClientSecretCreatedEventFromRowState(
                                state, client, digest,
                            );

                        yield nextEvent;
                        break;
                    }

                    case "delete": {
                        const digest = event.key.digest;
                        const client = event.key.client_id;
                        const nextEvent =
                            clientSecretRow.createClientSecretDeletedEventFromRowState(
                                lastState, client, digest,
                            );

                        yield nextEvent;
                        break;
                    }

                    default: throw new Error("unexpected row event type");
                }

                lastState = state;
            }
        }
        finally {
            services.clientSecretRowQuery.release(queryPromise);
        }
    }
}

//#endregion

//#region state / events

export interface ClientSecretEntity {
    created: string
}

export interface ClientSecretState {
    error: boolean;
    entities: immutable.Map<string, ClientSecretEntity>;
}

const initialState: ClientSecretState = {
    error: false,
    entities: immutable.Map<string, ClientSecretEntity>(),
};

export type ClientSecretEventUnion =
    authSpec.ClientSecretSnapshotSchema |
    authSpec.ClientSecretCreatedSchema |
    authSpec.ClientSecretDeletedSchema;

function reduce(
    state: ClientSecretState,
    event: ErrorEvent | ClientSecretEventUnion,
): ClientSecretState {
    if (isErrorEvent(event)) {
        return {
            ...state,
            error: true,
        };
    }

    switch (event.type) {
        case "client-secret-snapshot": {
            let { entities } = initialState;
            entities = entities.asMutable();

            for (
                const {
                    digest,
                    created,
                } of event.payload.clientSecrets
            ) {
                assert(!entities.has(digest), "client-secret already exist");

                const entity = {
                    created,
                };
                entities.set(digest, entity);
            }

            entities.asImmutable();

            return {
                error: false,
                entities,
            };
        }

        case "client-secret-created": {
            let { entities } = state;

            const {
                digest,
                created,
            } = event.payload.clientSecret;

            assert(!entities.has(digest), "client-secret already exist");

            const entity: ClientSecretEntity = {
                created,
            };
            entities = entities.set(digest, entity);

            return {
                ...state,
                entities,
            };
        }

        case "client-secret-deleted": {
            let { entities } = state;

            const { digest } = event.payload.clientSecret;

            assert(entities.has(digest), "client-secret does not exist");
            entities = entities.delete(digest);

            return {
                ...state,
                entities,
            };
        }

        default: return state;
    }
}

//#endregion

//#region selectors

export function selectClientSecretSnapshotEvent(
    state: ClientSecretState,
): authSpec.ClientSecretSnapshotSchema {
    const payload = {
        clientSecrets: state.entities.
            map(({ created }, digest) => ({
                digest,
                created,
            })).
            valueSeq().
            toArray(),
    };

    return {
        type: "client-secret-snapshot",
        payload,
    };
}

export function selectClientSecretExists(
    state: ClientSecretState,
    digest: string,
): boolean {
    return state.entities.has(digest);
}

export function selectClientSecretEntity(
    state: ClientSecretState,
    digest: string,
): ClientSecretEntity | undefined {
    const clients = state.entities;
    const entity = clients.get(digest);
    if (!entity) return;

    return entity;
}

//#endregion
