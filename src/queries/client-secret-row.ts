import * as authDb from "@latency.gg/lgg-auth-db";
import * as authSpec from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { convertPgBinaryToBuffer, createRowQueryFactory, QuerySource, RowQueryState } from "../utils/index.js";

//#region query

export type ClientSecretRowQuery = InstanceMemoizer<
    Promise<ClientSecretRowQuerySource>, [Partial<authDb.ClientSecretRow>]
>

export type ClientSecretRowQuerySource = QuerySource<
    ClientSecretRowState, authDb.ClientSecretRowEvent
>

export function createClientSecretRowQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "pgPool">,
    onError: (error: unknown) => void,
): ClientSecretRowQuery {
    return createRowQueryFactory({
        pool: services.pgPool,
        getPrimaryKey: key => makePrimaryKey(key),
        getIndexKey: {},
        onError,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
        createTableEvents: authDb.createClientSecretRowEvents,
    });

}

//#endregion

//#region state / events

export type ClientSecretRowState = RowQueryState<authDb.ClientSecretRow>;

//#endregion

//#region selectors

export function selectClientSecretRow(
    state: ClientSecretRowState,
    client: string,
    digest: string,
): authDb.ClientSecretRow | undefined {
    const key = makePrimaryKey({ client_id: client, digest });
    const row = state.rows.get(key);
    if (!row) return;

    return row;
}

export function createClientSecretSnapshotEventFromRowState(
    state: ClientSecretRowState,
): authSpec.ClientSecretSnapshotSchema {
    const payload = {
        clientSecrets: state.rows.
            map(row => mapRowToEntity(row)).
            valueSeq().
            toArray(),
    };

    return {
        type: "client-secret-snapshot",
        payload,
    };
}

export function createClientSecretCreatedEventFromRowState(
    state: ClientSecretRowState,
    client: string,
    digest: string,
): authSpec.ClientSecretCreatedSchema {
    const row = selectClientSecretRow(state, client, digest);
    assert(row != null, "expected row");

    const payload = {
        clientSecret: mapRowToEntity(row),
    };

    return {
        type: "client-secret-created",
        payload,
    };
}

export function createClientSecretDeletedEventFromRowState(
    state: ClientSecretRowState,
    client: string,
    digest: string,
): authSpec.ClientSecretDeletedSchema {
    const row = selectClientSecretRow(state, client, digest);
    assert(row != null, "expected row");

    const payload = {
        clientSecret: mapRowToEntity(row),
    };

    return {
        type: "client-secret-deleted",
        payload,
    };
}

//#endregion

//#region helpers

function makePrimaryKey(key: authDb.ClientSecretRowKey) {
    return [key.client_id, key.digest].join("\x1f");
}

function mapRowToEntity(
    row: authDb.ClientSecretRow,
): authSpec.ClientSecretEntitySchema {
    const digest = convertPgBinaryToBuffer(row.digest).toString("base64");
    const created = new Date(row.created_utc).toISOString();

    return {
        digest,
        created,
    };
}

//#endregion
