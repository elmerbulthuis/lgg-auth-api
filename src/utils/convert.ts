import assert from "assert";

export function convertPgBinaryToBuffer(pgHex: string) {
    assert(pgHex.startsWith("\\x"), "expected \\x");

    const hex = pgHex.substring(2);
    const buffer = Buffer.from(hex, "hex");
    return buffer;
}

export function convertPgBinaryFromBuffer(buffer: Buffer) {
    const hex = buffer.toString("hex");
    const pgHex = "\\x" + hex;
    return pgHex;
}

