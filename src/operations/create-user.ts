import * as authApi from "@latency.gg/lgg-auth-oas";
import * as crypto from "crypto";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createCreateUserOperation(
    context: application.Context,
): authApi.CreateUserOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const incomingEntity = await incomingRequest.entity();
        const userName = incomingEntity.name;
        const userIdBuffer = crypto.randomBytes(12);
        const userSaltBuffer = crypto.randomBytes(64);

        await withTransaction(
            context.services.pgPool,
            async pgClient => {

                const result = await pgClient.query(
                    `
insert into public.user(
    id, name, salt, created_utc
)
values (
    $1, $2, $3, $4
)
;
`,
                    [
                        userIdBuffer,
                        userName,
                        userSaltBuffer,
                        now.toISOString(),
                    ],
                );
            },
        );

        return {
            status: 201,
            parameters: {},
            entity() {
                return {
                    id: userIdBuffer.toString("base64"),
                };
            },
        };

    };
}
