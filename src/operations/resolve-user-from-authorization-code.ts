import * as authApi from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import * as crypto from "crypto";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createResolveUserFromAuthorizationCodeOperation(
    context: application.Context,
): authApi.ResolveUserFromAuthorizationCodeOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const incomingEntity = await incomingRequest.entity();

        const clientId = incomingRequest.parameters.client;
        const clientIdBuffer = Buffer.from(clientId, "base64");
        const code = incomingEntity.code;
        const codeBuffer = Buffer.from(code, "base64");

        const userIdBuffer = await withTransaction(
            context.services.pgPool,
            async pgClient => {
                let result;

                result = await pgClient.query(
                    `
select salt
from public.client
where id = $1
;
`,
                    [
                        clientIdBuffer,
                    ],
                );
                assert(result.rowCount === 1);

                const saltBuffer = result.rows[0].salt as Buffer;

                const codeHasher = crypto.createHash("sha512");
                codeHasher.update(codeBuffer);
                codeHasher.update(saltBuffer);
                const codeDigestBuffer = codeHasher.digest();

                result = await pgClient.query(
                    `
delete from public.authorization_code
where client_id = $1
and digest = $2
and $3 >= created_utc
and $3 < expires_utc
returning user_id
;
`,
                    [
                        clientIdBuffer,
                        codeDigestBuffer,
                        now.toISOString(),
                    ],
                );

                assert(result.rowCount === 1);

                const userIdBuffer = result.rows[0].user_id as Buffer;
                return userIdBuffer;
            },
        );

        return {
            status: 200,
            parameters: {},
            entity() {
                return {
                    user: userIdBuffer.toString("base64"),
                };
            },
        };
    };
}
