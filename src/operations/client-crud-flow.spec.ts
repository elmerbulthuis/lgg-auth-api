import assert from "assert";
import * as pg from "pg";
import { withTransaction } from "table-access";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("client crud flow", t => withContext(async context => {
    await withTransaction(context.services.pgPool, initializeMocks);

    const applicationClient = context.createApplicationClient();

    const subscribeClientResult = await applicationClient.subscribeClientEvents({
        parameters: {},
    });
    assert(subscribeClientResult.status === 200);

    const abortController = new AbortController();
    const eventIterable = subscribeClientResult.entities(abortController.signal);
    const eventIterator = eventIterable[Symbol.asyncIterator]();

    try {

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-snapshot");
            t.deepEqual(
                eventNext.value.payload.clients,
                [
                ],
            );
        }

        let clientId: string;
        let created: string;
        {
            const result = await applicationClient.createClient({
                parameters: {},
                entity() { return { name: "testing-123" }; },
            });
            assert(result.status === 201);
            const entity = await result.entity();
            clientId = entity.id;

            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-created");
            created = eventNext.value.payload.client.created;
            t.deepEqual(
                eventNext.value.payload.client,
                {
                    id: clientId,
                    name: "testing-123",
                    created,
                },
            );
        }

        {
            const result = await applicationClient.updateClient({
                parameters: {
                    client: clientId,
                },
                entity() { return { name: "testing-1234" }; },
            });
            assert(result.status === 204);

            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-updated");
            t.deepEqual(
                eventNext.value.payload.client,
                {
                    id: clientId,
                    name: "testing-1234",
                    created,
                },
            );
        }

        {
            const result = await applicationClient.deleteClient({
                parameters: {
                    client: clientId,
                },
            });
            assert(result.status === 204);

            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-deleted");
            t.deepEqual(
                eventNext.value.payload.client,
                {
                    id: clientId,
                    name: "testing-1234",
                    created,
                },
            );
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
    finally {
        abortController.abort();

        await eventIterator.next().catch(error => t.pass());
    }

    async function initializeMocks(client: pg.ClientBase) {
        await client.query("delete from public.client");
    }

}));
