import * as authApi from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import * as crypto from "crypto";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createCreateClientSecretOperation(
    context: application.Context,
): authApi.CreateClientSecretOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const incomingEntity = await incomingRequest.entity();
        const clientIdBuffer = Buffer.from(incomingRequest.parameters.client, "base64");
        const secretBuffer = crypto.randomBytes(60);

        const { secretDigestBuffer } = await withTransaction(
            context.services.pgPool,
            async pgClient => {

                let result;

                result = await pgClient.query(
                    `
select salt
from public.client
where id = $1
;
`,
                    [
                        clientIdBuffer,
                    ],
                );

                assert(result.rowCount === 1);

                const saltBuffer = result.rows[0].salt;

                const secretHasher = crypto.createHash("sha512");
                secretHasher.update(secretBuffer);
                secretHasher.update(saltBuffer);
                const secretDigestBuffer = secretHasher.digest();

                result = await pgClient.query(
                    `
insert into public.client_secret (
    client_id, digest, created_utc
)
values (
    $1, $2, $3
)
`,
                    [
                        clientIdBuffer,
                        secretDigestBuffer,
                        now.toISOString(),
                    ],
                );

                return { secretDigestBuffer };
            },
        );

        return {
            status: 201,
            parameters: {},
            entity() {
                return {
                    digest: secretDigestBuffer.toString("base64"),
                    secret: secretBuffer.toString("base64"),
                };
            },
        };
    };
}
