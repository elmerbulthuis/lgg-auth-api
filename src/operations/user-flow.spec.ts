import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("authentication-flow", t => withContext(async context => {
    const client = context.createApplicationClient();

    let clientId: string;
    {
        const result = await client.createClient({
            parameters: {},
            entity() { return { name: "testing" }; },
        });
        assert(result.status === 201);
        const entity = await result.entity();
        clientId = entity.id;
    }

    let userId: string;
    {
        const result = await client.createUser({
            parameters: {
                client: clientId,
            },
            entity() {
                return {
                    name: "henkie",
                };
            },
        });
        assert(result.status === 201);
        const entity = await result.entity();
        userId = entity.id;
    }

    let authorizationCode: string;
    {
        const result = await client.createAuthorizationCode({
            parameters: {
                client: clientId,
            },
            entity() {
                return {
                    user: userId,
                };
            },
        });
        assert(result.status === 201);
        const entity = await result.entity();
        authorizationCode = entity.code;
    }

    {
        const result = await client.resolveUserFromAuthorizationCode({
            parameters: {
                client: clientId,
            },
            entity() {
                return {
                    code: authorizationCode,
                };
            },
        });
        assert(result.status === 200);
        const entity = await result.entity();
        t.equal(entity.user, userId);
    }

}));
