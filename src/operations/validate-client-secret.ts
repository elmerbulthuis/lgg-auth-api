import * as authApi from "@latency.gg/lgg-auth-oas";
import * as crypto from "crypto";
import * as application from "../application/index.js";

export function createValidateClientSecretOperation(
    context: application.Context,
): authApi.ValidateClientSecretOperationHandler {
    return async function (incomingRequest) {
        const incomingEntity = await incomingRequest.entity();
        const clientIdBuffer = Buffer.from(incomingRequest.parameters.client, "base64");
        const clientSecretBuffer = Buffer.from(incomingEntity.value, "base64");

        let result;

        result = await context.services.pgPool.query(
            `
select salt
from public.client
where id = $1
`,
            [
                clientIdBuffer,
            ],
        );
        if (result.rowCount !== 1) {
            return {
                status: 404,
                parameters: {},
            };
        }

        const clientSaltBuffer = result.rows[0].salt;

        const clientSecretHasher = crypto.createHash("sha512");
        clientSecretHasher.update(clientSecretBuffer);
        clientSecretHasher.update(clientSaltBuffer);
        const clientSecretDigest = clientSecretHasher.digest();

        result = await context.services.pgPool.query(
            `
select 1
from public.client_secret
where client_id = $1
and digest = $2
`,
            [
                clientIdBuffer,
                clientSecretDigest,
            ],
        );
        if (result.rowCount !== 1) return {
            status: 200,
            parameters: {},
            entity() {
                return {
                    valid: false,
                };
            },
        };

        return {
            status: 200,
            parameters: {},
            entity() {
                return {
                    valid: true,
                };
            },
        };

    };
}
