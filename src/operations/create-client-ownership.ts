import * as authDb from "@latency.gg/lgg-auth-db";
import * as authApi from "@latency.gg/lgg-auth-oas";
import { queryInsert, withTransaction } from "table-access";
import * as application from "../application/index.js";
import { convertPgBinaryFromBuffer } from "../utils/index.js";

export function createCreateClientOwnershipOperation(
    context: application.Context,
): authApi.CreateClientOwnershipOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const { user, client } = incomingRequest.parameters;
        const incomingEntity = await incomingRequest.entity();

        const userBuffer = Buffer.from(user, "base64");
        const clientBuffer = Buffer.from(client, "base64");

        const userPgBinary = convertPgBinaryFromBuffer(userBuffer);
        const clientPgBinary = convertPgBinaryFromBuffer(clientBuffer);

        await withTransaction(
            context.services.pgPool,
            async pgClient => {
                await queryInsert(
                    pgClient,
                    authDb.clientOwnershipRowDescriptor,
                    {
                        user_id: userPgBinary,
                        client_id: clientPgBinary,
                        created_utc: now.toISOString(),
                    },
                );

            },
        );

        return {
            status: 201,
            parameters: {},
            entity() {
                return {};
            },
        };
    };
}
