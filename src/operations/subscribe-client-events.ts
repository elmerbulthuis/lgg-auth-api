import * as authApi from "@latency.gg/lgg-auth-oas";
import * as application from "../application/index.js";
import { selectClientSnapshotEvent } from "../queries/index.js";
import { mapQueryEvents } from "../utils/index.js";

export function createSubscribeClientEventsOperation(
    context: application.Context,
): authApi.SubscribeClientEventsOperationHandler {
    return function (incomingRequest) {

        return {
            status: 200,
            parameters: {},
            async * entities(signal) {
                const queryPromise = context.services.clientQuery.acquire();
                try {
                    const query = await queryPromise;
                    const state = query.getState();
                    yield selectClientSnapshotEvent(state);
                    yield* mapQueryEvents(query.fork(signal));
                }
                catch (error) {
                    if (!signal?.aborted) throw error;
                }
                finally {
                    context.services.clientQuery.release(
                        queryPromise,
                        context.config.linger,
                    );
                }
            },
        };
    };
}
