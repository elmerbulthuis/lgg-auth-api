import * as authDb from "@latency.gg/lgg-auth-db";
import * as authOas from "@latency.gg/lgg-auth-oas";
import { queryDelete, UnexpectedRowCountError, withTransaction } from "table-access";
import * as application from "../application/index.js";
import { convertPgBinaryFromBuffer } from "../utils/index.js";

export function createDeleteClientOwnershipOperation(
    context: application.Context,
): authOas.DeleteClientOwnershipOperationHandler {
    return async function (incomingRequest) {
        const { user, client } = incomingRequest.parameters;

        const userBuffer = Buffer.from(user, "base64");
        const clientBuffer = Buffer.from(client, "base64");

        const userPgBinary = convertPgBinaryFromBuffer(userBuffer);
        const clientPgBinary = convertPgBinaryFromBuffer(clientBuffer);

        try {
            await withTransaction(context.services.pgPool, async client => {
                await queryDelete(
                    client,
                    authDb.clientOwnershipRowDescriptor,
                    {
                        user_id: userPgBinary,
                        client_id: clientPgBinary,
                    },
                );
            });
        }
        catch (error) {
            if (error instanceof UnexpectedRowCountError) {
                return {
                    status: 404,
                    parameters: {},
                };
            }

            throw error;
        }

        return {
            status: 204,
            parameters: {},
        };
    };
}
