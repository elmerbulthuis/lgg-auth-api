import * as authDb from "@latency.gg/lgg-auth-db";
import assert from "assert";
import * as pg from "pg";
import { queryInsert, withTransaction } from "table-access";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("client-secret crud flow", t => withContext(async context => {
    await withTransaction(context.services.pgPool, initializeMocks);

    const applicationClient = context.createApplicationClient();

    const client = "qg==";
    const subscribeClientSecretResult = await applicationClient.subscribeClientSecretEvents({
        parameters: {
            client,
        },
    });
    assert(subscribeClientSecretResult.status === 200);

    const abortController = new AbortController();
    const eventIterable = subscribeClientSecretResult.entities(abortController.signal);
    const eventIterator = eventIterable[Symbol.asyncIterator]();

    try {

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-secret-snapshot");
            t.deepEqual(
                eventNext.value.payload.clientSecrets,
                [
                ],
            );
        }

        let digest: string;
        {
            const result = await applicationClient.createClientSecret({
                parameters: {
                    client,
                },
                entity() {
                    return {};
                },
            });
            assert(result.status === 201);
            const entity = await result.entity();
            digest = entity.digest;

            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-secret-created");
            t.deepEqual(
                eventNext.value.payload.clientSecret,
                {
                    digest,
                    created: eventNext.value.payload.clientSecret.created,
                },
            );
        }

        {
            const result = await applicationClient.deleteClientSecret({
                parameters: {
                    client,
                    digest,
                },
            });
            assert(result.status === 204);

            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-secret-deleted");
            t.deepEqual(
                eventNext.value.payload.clientSecret,
                {
                    digest,
                    created: eventNext.value.payload.clientSecret.created,
                },
            );
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
    finally {
        abortController.abort();
        await eventIterator.next().catch(error => t.pass());
    }

    async function initializeMocks(client: pg.ClientBase) {
        await queryInsert(
            client,
            authDb.clientRowDescriptor,
            {
                id: "\\xaa",
                name: "tes",
                salt: "\\x00",
                created_utc: "2022-04-12T17:17:50",
            },
        );
    }
}));
