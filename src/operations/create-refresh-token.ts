import * as authApi from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import * as crypto from "crypto";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createCreateRefreshTokenOperation(
    context: application.Context,
): authApi.CreateRefreshTokenOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const incomingEntity = await incomingRequest.entity();

        const expires = new Date(now.valueOf() + context.config.refreshTokenAge);
        const clientId = incomingRequest.parameters.client;
        const clientIdBuffer = Buffer.from(clientId, "base64");
        const userId = incomingEntity.user;
        const userIdBuffer = Buffer.from(userId, "base64");

        const tokenBuffer = await withTransaction(
            context.services.pgPool,
            async pgClient => {
                let result;

                /*
                get the salt that we need to digest all those tokens
                */

                result = await pgClient.query(
                    `
select salt
from public.client
where id = $1
;
`,
                    [
                        clientIdBuffer,
                    ],
                );
                assert(result.rowCount === 1);

                const saltBuffer = result.rows[0].salt as Buffer;

                const tokenBuffer = await new Promise<Buffer>(
                    (resolve, reject) => crypto.randomBytes(
                        512,
                        (error, value) => error ? reject(error) : resolve(value),
                    ),
                );
                const tokenHasher = crypto.createHash("sha512");
                tokenHasher.update(tokenBuffer);
                tokenHasher.update(saltBuffer);
                const tokenDigestBuffer = tokenHasher.digest();

                result = await pgClient.query(
                    `
insert into public.refresh_token(
    client_id, user_id,
    digest,
    created_utc, expires_utc
)
values (
    $1, $2,
    $3,
    $4, $5
)
returning 1
;`,
                    [
                        clientIdBuffer,
                        userIdBuffer,
                        tokenDigestBuffer,
                        now.toISOString(),
                        expires.toISOString(),
                    ],
                );
                assert(result.rowCount === 1);

                return tokenBuffer;
            },
        );

        return {
            status: 201,
            parameters: {},
            entity() {
                return {
                    token: tokenBuffer.toString("base64"),
                    expires: expires.valueOf(),
                };
            },
        };
    };
}
