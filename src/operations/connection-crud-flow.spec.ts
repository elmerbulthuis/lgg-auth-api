import * as authDb from "@latency.gg/lgg-auth-db";
import assert from "assert";
import * as pg from "pg";
import { queryInsert, withTransaction } from "table-access";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("connection crud flow", t => withContext(async context => {
    await withTransaction(context.services.pgPool, initializeMocks);

    const applicationClient = context.createApplicationClient();

    const provider = "test-provider";
    const subject = "xxyyzz";
    const user = "qg==";
    const subscribeConnectionResult = await applicationClient.subscribeConnectionEvents({
        parameters: {
            provider,
        },
    });
    assert(subscribeConnectionResult.status === 200);

    const abortController = new AbortController();
    const eventIterable = subscribeConnectionResult.entities(abortController.signal);
    const eventIterator = eventIterable[Symbol.asyncIterator]();

    try {

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "connection-snapshot");
            t.deepEqual(
                eventNext.value.payload.connections,
                [
                ],
            );
        }

        let created: string;
        {
            const result = await applicationClient.connectUserToProviderSubject({
                parameters: {
                    provider,
                    subject,
                    user,
                },
                entity() {
                    return {};
                },
            });
            assert(result.status === 201);
            const entity = await result.entity();

            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "connection-created");

            created = eventNext.value.payload.connection.created;
            t.deepEqual(
                eventNext.value.payload.connection,
                {
                    subject,
                    user,
                    created,
                },
            );
        }

        {
            const result = await applicationClient.disconnectUserFromProviderSubject({
                parameters: {
                    provider,
                    subject,
                    user,
                },
            });
            assert(result.status === 204);

            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "connection-deleted");
            t.deepEqual(
                eventNext.value.payload.connection,
                {
                    subject,
                    user,
                    created,
                },
            );
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
    finally {
        abortController.abort();
        await eventIterator.next().catch(error => t.pass());
    }

    async function initializeMocks(client: pg.ClientBase) {
        await queryInsert(
            client,
            authDb.userRowDescriptor,
            {
                id: "\\xaa",
                name: "test-user",
                salt: "\\x00",
                created_utc: "2022-04-12T14:52:00",
            },
        );
    }

}));
