import * as authApi from "@latency.gg/lgg-auth-oas";
import * as oas3ts from "@oas3/oas3ts-lib";
import * as operations from "../operations/index.js";
import { Context } from "./context.js";

export type Server = authApi.Server;

export function createServer(
    context: Context,
    onError: (error: unknown) => void,
) {
    const server = new authApi.Server({});

    server.registerMiddleware(oas3ts.createErrorMiddleware(onError));

    server.registerMiddleware(
        oas3ts.createHeartbeatMiddleware(),
    );

    server.registerMiddleware(
        async function (this, route, request, next) {
            const operation = route ?
                oas3ts.getOperationId(
                    authApi.metadata,
                    route.name,
                    request.method,
                ) :
                undefined;

            const stopTimer = context.metrics.operationDuration.startTimer({ operation });
            try {
                const response = await next(request);
                stopTimer({ status: response.status });
                return response;
            }
            catch (error) {
                stopTimer();

                throw error;
            }
        },
    );

    //#region client

    server.registerCreateClientOperation(
        operations.createCreateClientOperation(context),
    );
    server.registerDeleteClientOperation(
        operations.createDeleteClientOperation(context),
    );
    server.registerUpdateClientOperation(
        operations.createUpdateClientOperation(context),
    );
    server.registerSubscribeClientEventsOperation(
        operations.createSubscribeClientEventsOperation(context),
    );

    //#endregion

    //#region client-secret

    server.registerCreateClientSecretOperation(
        operations.createCreateClientSecretOperation(context),
    );
    server.registerDeleteClientSecretOperation(
        operations.createDeleteClientSecretOperation(context),
    );
    server.registerValidateClientSecret2Operation(
        operations.createValidateClientSecret2Operation(context),
    );

    server.registerSubscribeClientSecretEventsOperation(
        operations.createSubscribeClientSecretEventsOperation(context),
    );

    //#endregion

    //#region client-ownership

    server.registerCreateClientOwnershipOperation(
        operations.createCreateClientOwnershipOperation(context),
    );

    server.registerDeleteClientOwnershipOperation(
        operations.createDeleteClientOwnershipOperation(context),
    );

    server.registerSubscribeClientOwnershipEventsOperation(
        operations.createSubscribeClientOwnershipEventsOperation(context),
    );

    //#endregion

    //#region redirect

    server.registerCreateRedirectUriOperation(
        operations.createCreateRedirectUriOperation(context),
    );
    server.registerValidateRedirectUriOperation(
        operations.createValidateRedirectUriOperation(context),
    );

    //#endregion

    //#region user

    server.registerCreateUserOperation(
        operations.createCreateUserOperation(context),
    );

    server.registerResolveUserFromAuthorizationCodeOperation(
        operations.createResolveUserFromAuthorizationCodeOperation(context),
    );
    server.registerResolveUserFromProviderSubjectOperation(
        operations.createResolveUserFromProviderSubjectOperation(context),
    );

    //#endregion

    //#region connection

    server.registerConnectUserToProviderSubjectOperation(
        operations.createConnectUserToProviderSubjectOperation(context),
    );
    server.registerDisconnectUserFromProviderSubjectOperation(
        operations.createDisconnectUserFromProviderSubjectOperation(context),
    );

    server.registerSubscribeConnectionEventsOperation(
        operations.createSubscribeConnectionEventsOperation(context),
    );

    //#endregion

    //#region tokens

    server.registerCreateAuthorizationCodeOperation(
        operations.createAuthorizationCodeOperation(context),
    );
    server.registerCreateAccessTokenOperation(
        operations.createCreateAccessTokenOperation(context),
    );
    server.registerCreateRefreshTokenOperation(
        operations.createCreateRefreshTokenOperation(context),
    );

    //#endregion

    //#region deprecated

    server.registerValidateClientSecretOperation(
        operations.createValidateClientSecretOperation(context),
    );

    //#endregion

    return server;
}
